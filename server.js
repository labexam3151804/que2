const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors('*'))
app.use(express.json())

app.get('/', (request,response)=>{
    response.send('Server started inside container')
})
app.listen(1540,'0.0.0.0',()=>{
    console.log('Server started on 1540')
})